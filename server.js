/*
Node.js web server:
Node.js using Socket.io communication, and the Express.js framework.

*/

var express = require('express'),
    http = require('http');

var port = process.env.OPENSHIFT_NODEJS_PORT || 8080,
    ip = process.env.OPENSHIFT_NODEJS_IP || '127.0.0.1';

var app = express();
var fs = require('fs');
app.use(express.static(__dirname + '/public'));
var server = http.createServer(app);
var io = require('socket.io').listen(server);
server.listen(port, ip);

var clients = [];

var connectedUsers = 0;
var loadedUsersInRooms = {};

// Express.js:
// Create "public" directory, that will automatically be served to clients without hand-writing request handlers
app.get('/', function(req, res){
  res.sendfile('index.html');
});

io.sockets.on('connection', function (socket) {
    socket.emit('connect', { hello: 'world' });
    /*
    socket.on('my other event', function (data) {
        console.log(data);
    });
    */
    console.log("Player Joined");
    socket.emit("Test", "CONNECTION_SUCCESS");

    var roomName = "";
    socket.on("joinRoom", function () {
        // assign the player to a room, or generate a new room if necessary
        var rooms = io.sockets.manager.rooms;
        var foundRoom = false;
        for (room in rooms) {
            if (room == "") continue;
            console.log(room);
            if (rooms[room].length < 2) {
                socket.join(room.replace('/', ""));
                roomName = room.replace('/', "");
                foundRoom = true;
                break;
            }
        }
        // if no room found, make a new one
        if (!foundRoom) {
            roomName = new Date().getTime();
            socket.join(roomName);
            loadedUsersInRooms[roomName] = 0;
        }
        console.log("Client joined room: " + roomName);

        if (io.sockets.clients(roomName).length == 2) {
            var units = [];
  
            units.push({ name: "Bob", x: 0, y: 7, id: 0, class: "Unit", facing: 1, player: 0 });
            units.push({ name: "Jack", x: 18, y: 0, id: 1, class: "Unit", facing: 0, player: 1 });
            units.push({ name: "Arthur", x: 1, y: 8, id: 2, class: "Knight", facing: 1, player: 0 });

            units.push({ name: "Jeremy", x: 16, y: 0, id: 3, class: "Knight", facing: 0, player: 1 });
            units.push({ name: "Marisa", x: 0, y: 8, id: 4, class: "Witch", facing: 1, player: 0 });
            units.push({ name: "Selena", x: 17, y: 2, id: 5, class: "Witch", facing: 0, player: 1 });
            io.sockets.clients(roomName).forEach(function (s, i) {
                // Game init:
                // - Define which map to load
                // - Define units and start positions
                var dataObj = {};
                dataObj.playerIndex = i;
                // Send map JSON to client.
                // In final product, this would either pick from a random list of maps, or be specified by one of the players
                dataObj.map = '[[{"x":0,"y":0,"h":-4,"type":0,"walkable":true},{"x":1,"y":0,"h":-4,"type":0,"walkable":true},{"x":2,"y":0,"h":-3,"type":5,"walkable":true},{"x":3,"y":0,"h":-3,"type":5,"walkable":true},{"x":4,"y":0,"h":-2,"type":5,"walkable":true},{"x":5,"y":0,"h":-2,"type":5,"walkable":true},{"x":6,"y":0,"h":-2,"type":5,"walkable":true},{"x":7,"y":0,"h":-1,"type":5,"walkable":true},{"x":8,"y":0,"h":-1,"type":5,"walkable":true},{"x":9,"y":0,"h":0,"type":5,"walkable":true},{"x":10,"y":0,"h":0,"type":5,"walkable":true},{"x":11,"y":0,"h":0,"type":5,"walkable":true},{"x":12,"y":0,"h":0,"type":5,"walkable":true},{"x":13,"y":0,"h":1,"type":5,"walkable":true},{"x":14,"y":0,"h":1,"type":5,"walkable":true},{"x":15,"y":0,"h":1,"type":5,"walkable":true},{"x":16,"y":0,"h":0,"type":5,"walkable":true},{"x":17,"y":0,"h":0,"type":1,"walkable":true},{"x":18,"y":0,"h":-1,"type":5,"walkable":true},{"x":19,"y":0,"h":0,"type":5,"walkable":true},{"x":20,"y":0,"h":0,"type":5,"walkable":true},{"x":21,"y":0,"h":-3,"type":2,"walkable":true},{"x":22,"y":0,"h":1,"type":1,"walkable":true},{"x":23,"y":0,"h":1,"type":1,"walkable":true},{"x":24,"y":0,"h":-3,"type":2,"walkable":true}],[{"x":0,"y":1,"h":-4,"type":0,"walkable":true},{"x":1,"y":1,"h":-3,"type":5,"walkable":true},{"x":2,"y":1,"h":-3,"type":5,"walkable":true},{"x":3,"y":1,"h":-2,"type":5,"walkable":true},{"x":4,"y":1,"h":-2,"type":5,"walkable":true},{"x":5,"y":1,"h":-1,"type":5,"walkable":true},{"x":6,"y":1,"h":-1,"type":5,"walkable":true},{"x":7,"y":1,"h":-1,"type":5,"walkable":true},{"x":8,"y":1,"h":-1,"type":5,"walkable":true},{"x":9,"y":1,"h":0,"type":5,"walkable":true},{"x":10,"y":1,"h":0,"type":5,"walkable":true},{"x":11,"y":1,"h":0,"type":5,"walkable":true},{"x":12,"y":1,"h":0,"type":5,"walkable":true},{"x":13,"y":1,"h":1,"type":5,"walkable":true},{"x":14,"y":1,"h":1,"type":5,"walkable":true},{"x":15,"y":1,"h":1,"type":5,"walkable":true},{"x":16,"y":1,"h":1,"type":5,"walkable":true},{"x":17,"y":1,"h":0,"type":5,"walkable":true},{"x":18,"y":1,"h":0,"type":1,"walkable":true},{"x":19,"y":1,"h":0,"type":5,"walkable":true},{"x":20,"y":1,"h":1,"type":5,"walkable":true},{"x":21,"y":1,"h":1,"type":5,"walkable":true},{"x":22,"y":1,"h":1,"type":1,"walkable":true},{"x":23,"y":1,"h":1,"type":1,"walkable":true},{"x":24,"y":1,"h":1,"type":5,"walkable":true}],[{"x":0,"y":2,"h":-4,"type":5,"walkable":true},{"x":1,"y":2,"h":-3,"type":5,"walkable":true},{"x":2,"y":2,"h":-2,"type":5,"walkable":true},{"x":3,"y":2,"h":-2,"type":5,"walkable":true},{"x":4,"y":2,"h":-1,"type":5,"walkable":true},{"x":5,"y":2,"h":-1,"type":1,"walkable":true},{"x":6,"y":2,"h":-1,"type":1,"walkable":true},{"x":7,"y":2,"h":-1,"type":1,"walkable":true},{"x":8,"y":2,"h":-1,"type":1,"walkable":true},{"x":9,"y":2,"h":0,"type":1,"walkable":true},{"x":10,"y":2,"h":0,"type":1,"walkable":true},{"x":11,"y":2,"h":0,"type":1,"walkable":true},{"x":12,"y":2,"h":0,"type":1,"walkable":true},{"x":13,"y":2,"h":1,"type":1,"walkable":true},{"x":14,"y":2,"h":1,"type":1,"walkable":true},{"x":15,"y":2,"h":1,"type":5,"walkable":true},{"x":16,"y":2,"h":1,"type":5,"walkable":true},{"x":17,"y":2,"h":1,"type":5,"walkable":true},{"x":18,"y":2,"h":1,"type":5,"walkable":true},{"x":19,"y":2,"h":1,"type":5,"walkable":true},{"x":20,"y":2,"h":1,"type":5,"walkable":true},{"x":21,"y":2,"h":1,"type":5,"walkable":true},{"x":22,"y":2,"h":1,"type":1,"walkable":true},{"x":23,"y":2,"h":1,"type":1,"walkable":true},{"x":24,"y":2,"h":1,"type":5,"walkable":true}],[{"x":0,"y":3,"h":-3,"type":5,"walkable":true},{"x":1,"y":3,"h":-3,"type":5,"walkable":true},{"x":2,"y":3,"h":-2,"type":5,"walkable":true},{"x":3,"y":3,"h":-2,"type":5,"walkable":true},{"x":4,"y":3,"h":-1,"type":5,"walkable":true},{"x":5,"y":3,"h":-1,"type":1,"walkable":true},{"x":6,"y":3,"h":-1,"type":1,"walkable":true},{"x":7,"y":3,"h":-1,"type":1,"walkable":true},{"x":8,"y":3,"h":-1,"type":1,"walkable":true},{"x":9,"y":3,"h":0,"type":1,"walkable":true},{"x":10,"y":3,"h":0,"type":1,"walkable":true},{"x":11,"y":3,"h":0,"type":1,"walkable":true},{"x":12,"y":3,"h":0,"type":1,"walkable":true},{"x":13,"y":3,"h":1,"type":1,"walkable":true},{"x":14,"y":3,"h":1,"type":1,"walkable":true},{"x":15,"y":3,"h":1,"type":5,"walkable":true},{"x":16,"y":3,"h":1,"type":5,"walkable":true},{"x":17,"y":3,"h":1,"type":5,"walkable":true},{"x":18,"y":3,"h":1,"type":5,"walkable":true},{"x":19,"y":3,"h":1,"type":5,"walkable":true},{"x":20,"y":3,"h":1,"type":5,"walkable":true},{"x":21,"y":3,"h":1,"type":5,"walkable":true},{"x":22,"y":3,"h":1,"type":1,"walkable":true},{"x":23,"y":3,"h":1,"type":1,"walkable":true},{"x":24,"y":3,"h":1,"type":5,"walkable":true}],[{"x":0,"y":4,"h":-3,"type":5,"walkable":true},{"x":1,"y":4,"h":-2,"type":5,"walkable":true},{"x":2,"y":4,"h":-2,"type":5,"walkable":true},{"x":3,"y":4,"h":-1,"type":5,"walkable":true},{"x":4,"y":4,"h":-1,"type":5,"walkable":true},{"x":5,"y":4,"h":-1,"type":1,"walkable":true},{"x":6,"y":4,"h":-1,"type":1,"walkable":true},{"x":7,"y":4,"h":-1,"type":5,"walkable":true},{"x":8,"y":4,"h":-1,"type":5,"walkable":true},{"x":9,"y":4,"h":0,"type":5,"walkable":true},{"x":10,"y":4,"h":0,"type":5,"walkable":true},{"x":11,"y":4,"h":0,"type":5,"walkable":true},{"x":12,"y":4,"h":0,"type":5,"walkable":true},{"x":13,"y":4,"h":1,"type":1,"walkable":true},{"x":14,"y":4,"h":1,"type":1,"walkable":true},{"x":15,"y":4,"h":1,"type":1,"walkable":true},{"x":16,"y":4,"h":1,"type":1,"walkable":true},{"x":17,"y":4,"h":1,"type":1,"walkable":true},{"x":18,"y":4,"h":1,"type":1,"walkable":true},{"x":19,"y":4,"h":1,"type":1,"walkable":true},{"x":20,"y":4,"h":1,"type":1,"walkable":true},{"x":21,"y":4,"h":1,"type":1,"walkable":true},{"x":22,"y":4,"h":1,"type":1,"walkable":true},{"x":23,"y":4,"h":1,"type":1,"walkable":true},{"x":24,"y":4,"h":1,"type":5,"walkable":true}],[{"x":0,"y":5,"h":-2,"type":5,"walkable":true},{"x":1,"y":5,"h":-2,"type":5,"walkable":true},{"x":2,"y":5,"h":-2,"type":5,"walkable":true},{"x":3,"y":5,"h":-1,"type":5,"walkable":true},{"x":4,"y":5,"h":-1,"type":5,"walkable":true},{"x":5,"y":5,"h":-1,"type":1,"walkable":true},{"x":6,"y":5,"h":-1,"type":1,"walkable":true},{"x":7,"y":5,"h":-1,"type":5,"walkable":true},{"x":8,"y":5,"h":0,"type":5,"walkable":true},{"x":9,"y":5,"h":0,"type":5,"walkable":true},{"x":10,"y":5,"h":0,"type":5,"walkable":true},{"x":11,"y":5,"h":0,"type":5,"walkable":true},{"x":12,"y":5,"h":1,"type":5,"walkable":true},{"x":13,"y":5,"h":1,"type":1,"walkable":true},{"x":14,"y":5,"h":1,"type":1,"walkable":true},{"x":15,"y":5,"h":1,"type":1,"walkable":true},{"x":16,"y":5,"h":1,"type":1,"walkable":true},{"x":17,"y":5,"h":1,"type":1,"walkable":true},{"x":18,"y":5,"h":1,"type":1,"walkable":true},{"x":19,"y":5,"h":1,"type":1,"walkable":true},{"x":20,"y":5,"h":1,"type":1,"walkable":true},{"x":21,"y":5,"h":1,"type":1,"walkable":true},{"x":22,"y":5,"h":1,"type":1,"walkable":true},{"x":23,"y":5,"h":1,"type":1,"walkable":true},{"x":24,"y":5,"h":1,"type":5,"walkable":true}],[{"x":0,"y":6,"h":-6,"type":2,"walkable":true},{"x":1,"y":6,"h":-2,"type":5,"walkable":true},{"x":2,"y":6,"h":-2,"type":5,"walkable":true},{"x":3,"y":6,"h":-2,"type":5,"walkable":true},{"x":4,"y":6,"h":-2,"type":5,"walkable":true},{"x":5,"y":6,"h":-1,"type":1,"walkable":true},{"x":6,"y":6,"h":-1,"type":1,"walkable":true},{"x":7,"y":6,"h":0,"type":5,"walkable":true},{"x":8,"y":6,"h":0,"type":5,"walkable":true},{"x":9,"y":6,"h":0,"type":5,"walkable":true},{"x":10,"y":6,"h":0,"type":5,"walkable":true},{"x":11,"y":6,"h":1,"type":5,"walkable":true},{"x":12,"y":6,"h":1,"type":5,"walkable":true},{"x":13,"y":6,"h":1,"type":5,"walkable":true},{"x":14,"y":6,"h":1,"type":5,"walkable":true},{"x":15,"y":6,"h":1,"type":5,"walkable":true},{"x":16,"y":6,"h":1,"type":5,"walkable":true},{"x":17,"y":6,"h":1,"type":5,"walkable":true},{"x":18,"y":6,"h":1,"type":5,"walkable":true},{"x":19,"y":6,"h":1,"type":5,"walkable":true},{"x":20,"y":6,"h":1,"type":5,"walkable":true},{"x":21,"y":6,"h":1,"type":5,"walkable":true},{"x":22,"y":6,"h":1,"type":5,"walkable":true},{"x":23,"y":6,"h":1,"type":5,"walkable":true},{"x":24,"y":6,"h":1,"type":5,"walkable":true}],[{"x":0,"y":7,"h":-2,"type":1,"walkable":true},{"x":1,"y":7,"h":-2,"type":1,"walkable":true},{"x":2,"y":7,"h":-2,"type":1,"walkable":true},{"x":3,"y":7,"h":-2,"type":1,"walkable":true},{"x":4,"y":7,"h":-2,"type":1,"walkable":true},{"x":5,"y":7,"h":-1,"type":1,"walkable":true},{"x":6,"y":7,"h":-1,"type":1,"walkable":true},{"x":7,"y":7,"h":0,"type":5,"walkable":true},{"x":8,"y":7,"h":0,"type":5,"walkable":true},{"x":9,"y":7,"h":0,"type":5,"walkable":true},{"x":10,"y":7,"h":1,"type":5,"walkable":true},{"x":11,"y":7,"h":1,"type":5,"walkable":true},{"x":12,"y":7,"h":1,"type":5,"walkable":true},{"x":13,"y":7,"h":1,"type":5,"walkable":true},{"x":14,"y":7,"h":1,"type":5,"walkable":true},{"x":15,"y":7,"h":2,"type":5,"walkable":true},{"x":16,"y":7,"h":2,"type":1,"walkable":true},{"x":17,"y":7,"h":2,"type":1,"walkable":true},{"x":18,"y":7,"h":2,"type":1,"walkable":true},{"x":19,"y":7,"h":2,"type":1,"walkable":true},{"x":20,"y":7,"h":2,"type":1,"walkable":true},{"x":21,"y":7,"h":2,"type":1,"walkable":true},{"x":22,"y":7,"h":2,"type":5,"walkable":true},{"x":23,"y":7,"h":1,"type":5,"walkable":true},{"x":24,"y":7,"h":1,"type":5,"walkable":true}],[{"x":0,"y":8,"h":-2,"type":1,"walkable":true},{"x":1,"y":8,"h":-2,"type":1,"walkable":true},{"x":2,"y":8,"h":-2,"type":1,"walkable":true},{"x":3,"y":8,"h":-2,"type":1,"walkable":true},{"x":4,"y":8,"h":-2,"type":1,"walkable":true},{"x":5,"y":8,"h":-1,"type":1,"walkable":true},{"x":6,"y":8,"h":-1,"type":1,"walkable":true},{"x":7,"y":8,"h":0,"type":5,"walkable":true},{"x":8,"y":8,"h":0,"type":5,"walkable":true},{"x":9,"y":8,"h":1,"type":5,"walkable":true},{"x":10,"y":8,"h":1,"type":5,"walkable":true},{"x":11,"y":8,"h":1,"type":5,"walkable":true},{"x":12,"y":8,"h":1,"type":5,"walkable":true},{"x":13,"y":8,"h":2,"type":5,"walkable":true},{"x":14,"y":8,"h":2,"type":1,"walkable":true},{"x":15,"y":8,"h":3,"type":3,"walkable":true},{"x":16,"y":8,"h":3,"type":3,"walkable":true},{"x":17,"y":8,"h":3,"type":3,"walkable":true},{"x":18,"y":8,"h":3,"type":3,"walkable":true},{"x":19,"y":8,"h":3,"type":3,"walkable":true},{"x":20,"y":8,"h":3,"type":3,"walkable":true},{"x":21,"y":8,"h":3,"type":3,"walkable":true},{"x":22,"y":8,"h":3,"type":3,"walkable":true},{"x":23,"y":8,"h":2,"type":1,"walkable":true},{"x":24,"y":8,"h":2,"type":5,"walkable":true}],[{"x":0,"y":9,"h":-6,"type":2,"walkable":true},{"x":1,"y":9,"h":-2,"type":5,"walkable":true},{"x":2,"y":9,"h":-2,"type":5,"walkable":true},{"x":3,"y":9,"h":-2,"type":5,"walkable":true},{"x":4,"y":9,"h":-2,"type":5,"walkable":true},{"x":5,"y":9,"h":-1,"type":5,"walkable":true},{"x":6,"y":9,"h":0,"type":5,"walkable":true},{"x":7,"y":9,"h":0,"type":5,"walkable":true},{"x":8,"y":9,"h":0,"type":5,"walkable":true},{"x":9,"y":9,"h":1,"type":5,"walkable":true},{"x":10,"y":9,"h":1,"type":5,"walkable":true},{"x":11,"y":9,"h":1,"type":5,"walkable":true},{"x":12,"y":9,"h":1,"type":5,"walkable":true},{"x":13,"y":9,"h":2,"type":1,"walkable":true},{"x":14,"y":9,"h":3,"type":3,"walkable":true},{"x":15,"y":9,"h":3,"type":3,"walkable":true},{"x":16,"y":9,"h":3,"type":3,"walkable":true},{"x":17,"y":9,"h":3,"type":3,"walkable":true},{"x":18,"y":9,"h":3,"type":3,"walkable":true},{"x":19,"y":9,"h":3,"type":3,"walkable":true},{"x":20,"y":9,"h":3,"type":3,"walkable":true},{"x":21,"y":9,"h":3,"type":3,"walkable":true},{"x":22,"y":9,"h":3,"type":3,"walkable":true},{"x":23,"y":9,"h":3,"type":3,"walkable":true},{"x":24,"y":9,"h":2,"type":1,"walkable":true}]]';
                dataObj.units = units;
                console.log("Init game to socket " + i);
                s.emit("loadGame", dataObj);
            });
        }
    });

    // Character move
    socket.on('charMove', function (data) {
        socket.broadcast.to(roomName).emit('charMoved', data);
    });
    // undo move
    socket.on('undoCharMove', function (data) {
        socket.broadcast.to(roomName).emit('undoMove', data);
    });

    // Character end turn
    socket.on('endTurn', function (data) {
        socket.broadcast.to(roomName).emit('turnEnded', data);
    });
    socket.on('attack', function (data) {
        socket.broadcast.to(roomName).emit('executeAttack', data);
    });
    
    // Disconnect
    socket.on('disconnect', function() {
        console.log("user disconnected...");
        var nameFound = false;
        // Get socket name and display to users
        socket.get('name', function(e, name) {
            for (var i=0; i < clients.length; i++) {
                clients[i].get('name', function(e, n) {
                    if (n == name) {
                        nameFound = true;
                        clients.splice(i, 1);
                        var clientNames = getClientNames();
                        io.sockets.emit('userLeft', {name: name, userList: clientNames });
                    }
                });
                if (nameFound) break;
            }  
        });
    });
    
    
});
            
function getClientNames() {
    var clientNames = [];
    for (var i=0; i < clients.length; i++) {
        clients[i].get('name', function(e, name) {
            clientNames.push(name);
        });
    }
    return clientNames;
}
console.log("Server started");