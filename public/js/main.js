var Resources;
$(document).ready(function () {
    var Loader = function (imgUrls) { 
        this.images = {};
        this.imagesLoaded = 0;
        this.imagesFailed = 0;
        this.imageUrls = imgUrls; //[{'url': url, 'name': name }, ]
        this.imagesIndex = 0;
        this.tileImages = 0;
    };
    
    Loader.prototype = {
        loadImage: function(url, name) {
           var image = new Image(),
               self = this;
            image.src = url;
            image.addEventListener('load', function(e) {
                self.imagesLoaded ++;
            });
            image.addEventListener('error', function(e) {
               self.imagesFailed ++; 
            });
            this.images[name] = image;
            if (name.indexOf("tile") > -1) { self.tileImages ++; }
        },
        loadImages: function() {
            if (this.imagesIndex < this.imageUrls.length) {
                this.loadImage(this.imageUrls[this.imagesIndex].url, this.imageUrls[this.imagesIndex].name);
                this.imagesIndex ++;
            }
            return (this.imagesLoaded + this.imagesFailed) / this.imageUrls.length * 100;     
        },
        initImageAssets: function() {
            var self = this,
                loadingPercent = 0,
                interval = setInterval(function(e) {
                    loadingPercent = self.loadImages();
                    if (loadingPercent === 100) {
                        clearInterval(interval);
                        Resources = self.images;
                        Resources["numTiles"] = self.tileImages;
                        ImagesLoadedCallBack();
                    }
                }, 16);
        }
    };
    // Load all resources
    var E = new Engine("canvas"),
        imgUrls = [/*{'url': "img/tile.png", "name": "tile"},*/
                    {'url': "img/tiletypegrass.png", "name": "tile0"},
                    {'url': "img/tiletypedirt.png", "name": "tile1"},
                    {'url': "img/tiletypestone.png", "name": "tile2"},
                    {'url': "img/tiletypewater.png", "name": "tile3"},
                    {'url': "img/tiletypesand.png", "name": "tile4"},
                    {'url': "img/tiletypesnow.png", "name": "tile5"},
                    
                    {'url': "img/char.png", "name": "playerSprite"},
                    {'url': "img/enemyUnit.png", "name": "enemySprite"},
            
                    {'url': "img/attack.png", "name": "attackUI"},
                    {'url': "img/undo.png", "name": "undo"},
            
                    {'url': "img/turnIndicator.png", "name": "turnIndicator"},
                    {'url': "img/turnIndicatorEnemy.png", "name": "turnIndicatorEnemy"},
                    {'url': "img/tileOverlay.png", "name": "tileOverlay"},
                    {'url': "img/tileSelected.png", "name": "tileSelected"}];
    var L = new Loader(imgUrls);
    L.initImageAssets();
    
    function ImagesLoadedCallBack() {
        console.log("Assets Loaded");
        E.start();   
    }
    
});