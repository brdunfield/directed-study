/* ~~~~~~~~~~ DEBUG ~~~~~~~~~~~~~*/
var Test = function(name, x, y, id, stats, imgName, facing) {
    Unit.call(this, name, x, y, id, stats, imgName, facing);
    this.maxHP = 5;
    this.hp = 5;
    this.maxMP = 5;
    this.mp = 5;
};
Test.prototype = new Unit();
Test.prototype.parent = Unit.prototype;

/* ~~~~~~~~~~ END DEBUG ~~~~~~~~~~~~~*/

var Knight = function(name, x, y, id, stats, imgName, facing) {
    Unit.call(this, name, x, y, id, stats, imgName, facing);
    this.maxHP = 200;
    this.hp = 200;
    this.maxMP = 50;
    this.mp = 50;
    this.move = 4;
};
Knight.prototype = new Unit();
Knight.prototype.parent = Unit.prototype;



var Witch = function(name, x, y, id, stats, imgName, facing) {
    Unit.call(this, name, x, y, id, stats, imgName, facing);
    this.maxHP = 75;
    this.hp = 75;
    this.maxMP = 100;
    this.mp = 100;
};
Witch.prototype = new Unit();
Witch.prototype.parent = Unit.prototype;
Witch.prototype.levelUp = function() {
    Unit.prototype.levelUp.call(this);
    this.maxHP += 5;
    this.maxMP += 5;
}