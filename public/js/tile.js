var Tile = function (x, y, totY, h, type, walkable) {
    this.x = x;
    this.y = y;
    this.h = h;
    this.type = type;
    
    this.renderX = 50 * (x - y + totY);
    this.renderY = 25 * (x + y + h);
    var centerX = this.renderX + 50, // TODO, make 50 a global variable / variable somewhere (dx)
        centerY = this.renderY + 75; // 50 + dy (dy =25),
    this.isMoveOverlay = false;
    this.isAttackOverlay = false;
    this.isSelected = false;
    this.OccupiedBy = null;
    this.walkable = walkable;
    
    this.CheckPoint = function (cx, cy) {
        var Px = Math.abs(cx - centerX),
            Py = Math.abs(cy - centerY);
        
        return (((Px / 50) + (Py / 25)) <= 1);
    };
    
    this.RaiseHeight = function() {
        this.renderY -=25;
        centerY -= 25;
        this.h -= 1;
    };
    this.LowerHeight = function() {
        this.renderY +=25;
        centerY += 25;
        this.h += 1;
    };
};

Tile.prototype.distTo = function (t2) {
    var dx = Math.abs(t2.x - this.x);
    var dy = Math.abs(t2.y - this.y);
    return (dx + dy);
};

Tile.prototype.isClickable = function() {
    return (this.isMoveOverlay || this.isAttackOverlay || this.isSelected);
};

Tile.prototype.isWalkable = function() {
    return (this.walkable && this.OccupiedBy == null);
}