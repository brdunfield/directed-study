/* Socket Functions for online play */
var Socket = function(url, context) {
    var self = this;
    var socket = io.connect(url);
    // Pass context from the Engine so I can access its functions
    this.engine = context;
    socket.on('connect', function (data) {
        if (!data) return;
        console.log(data);
        //
        self.engine.connecting = true;
        socket.emit("joinRoom");
    });

    socket.on('loadGame', function (dataObj) {
        self.engine.map = self.engine.generateMap(dataObj.map);
        console.log("Map Loaded");
        // Parse units blob
        //console.log(dataObj.units)
        for (i in dataObj.units) {
            var u = dataObj.units[i];
            switch (u.class) {
                case "Unit":
                    if (u.player == dataObj.playerIndex)
                        self.engine.addPlayer(new Unit(u.name, u.x, u.y, u.id, null, "player", u.facing));
                    else
                        self.engine.addEnemy(new Unit(u.name, u.x, u.y, u.id, null, "enemy", u.facing));
                    break;
                case "Knight":
                    if (u.player == dataObj.playerIndex)
                        self.engine.addPlayer(new Knight(u.name, u.x, u.y, u.id, null, "player", u.facing));
                    else
                        self.engine.addEnemy(new Knight(u.name, u.x, u.y, u.id, null, "enemy", u.facing));
                    break;
                case "Witch":
                    if (u.player == dataObj.playerIndex)
                        self.engine.addPlayer(new Witch(u.name, u.x, u.y, u.id, null, "player", u.facing));
                    else
                        self.engine.addEnemy(new Witch(u.name, u.x, u.y, u.id, null, "enemy", u.facing));
                    break;
            }
        }
        console.log("Units loaded");

        if (self.engine.map.length > 0 && self.engine.player.length > 0 && self.engine.enemy.length > 0) {
            self.engine.connecting = false;
            self.engine.battle = true;
        }
    });

    socket.on('charMoved', function (dataObj) {
        var a = self.engine.units[dataObj.id];
        a.oldX = a.x;
        a.oldY = a.y;
        self.engine.charMoving = true;
        self.engine.movePath = dataObj.path;
        self.engine.moveDest = dataObj.dest;

    });

    socket.on('turnEnded', function (dataObj) {
        // server sends id of unit to end turn
        var id = dataObj.id;
        self.engine.units[self.engine.currTurn].EndTurn();
    });

    socket.on('executeAttack', function (dataObj) {
        var attacker = self.engine.units[dataObj.attacker];
        var tile = self.engine.map[dataObj.y][dataObj.x];
        self.engine.executeAttack(attacker, tile);
    });

    socket.on('undoMove', function (dataObj) {
        var unit = self.engine.units[dataObj.id];
        self.engine.undoMove(unit);
    });


    // Send Message
    this.sendmsg = function (msgName, data) {
        socket.emit(msgName, data);
    };
};


/* =======================================

Engine online functions

========================================== */