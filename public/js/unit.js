var Unit = function(name, x, y, id, stats, imgName, facing) {
    this.name = name;
    this.x = x;
    this.y = y;
    this.oldX = x;
    this.oldY = y;
    this.renderX = 0;
    this.renderY = 0;
    this.facing = facing;
    this.imgName = imgName;
    this.id = "unit" + id;
    this.dead = false;
    
    this.isMoveMode = true;
    this.isSkillMode = false;
    
    this.isMyTurn = false;
    this.hasMoved = true;
    this.hasActed = true;
    
    // Stats ~~~~
    this.level = 1;
    this.expUntilNext = 100;
    this.move = 3;
    this.jump = 2;
    
    this.maxHP = 100;
    this.hp = 100;
    this.maxMP = 50;
    this.mp = 50;
    
    this.str = 10;
    this.def = 3;
    
    this.atkRange = 1;
    
    this.CheckPoint = function (cx, cy) {
        var checkX = cx >= this.renderX + 20 && cx < this.renderX + 80,
            checkY = cy >= this.renderY + 10 && cy < this.renderY + 90;
        return (checkX && checkY);
    };
    
    this.updateRenderCoords = function (tile) {
        this.renderX = tile.renderX;
        this.renderY = tile.renderY;
    };
};

Unit.prototype.Attack = function (target) {
    target.hp -= (this.str - target.def);
    this.checkExp(target);
    
    this.hasActed = true;  
    this.isSkillMode = false;
    if (!this.hasMoved) { this.isMoveMode = true; }
};
Unit.prototype.EndTurn = function() {
    this.isMyTurn = false;  
};
Unit.prototype.StartTurn = function() {
    this.isMyTurn = true;  
    this.hasMoved = false;
    this.hasActed = false;
    this.isMoveMode = true;
    this.isSkillMode = false;
};
Unit.prototype.isStillMyTurn = function() {
    return !(this.hasMoved && this.hasActed);  
};

Unit.prototype.checkExp = function(target) {
    // TODO: Gain Exp here
    if (target.hp <=0) {
        this.expUntilNext -= 100; // DEBUG
        
        //this.expUntilNext -= parseInt(Math.random() * target.level) + target.level;
        if (this.expUntilNext <= 0) { this.levelUp(); }
    }    
};
Unit.prototype.levelUp = function() {
    this.level ++;
    this.expUntilNext = this.level*100;
    console.log(this.name + " has leveled up! New level: " + this.level + ".");
};