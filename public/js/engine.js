var Engine = function (canvasID) {
    var self = this,
        canvas = document.getElementById(canvasID);
    canvas.width = getWidth();
    canvas.height = getHeight();
    this.context = canvas.getContext('2d');
    this.translateX = 0;
    this.translateY = 0;
    this.dragCoords = [0, 0];
    this.isDragging = false;
    self.mouse = [];
    
    this.map = null;
    this.charMoving = false;
    this.movePath = [];
    this.moveDest = null;
    
    this.units = {};
    this.player = [];
    this.enemy = [];
    
    this.selectedChar = null;
    this.currTurn = "unit0";
    this.clicksTargetTiles = false;
    
    // Game menu / state variables
    this.battle = false;
    
    // Event handlers
    /*
    $("#canvas").click(function(e) {
        self.handleClick(e);
    });
    */
    $("#canvas").mousedown(function(e) {
        self.dragCoords = [e.pageX - self.translateX, e.pageY - self.translateY];
        self.isDragging = true;
        self.mouse = [e.pageX, e.pageY];
        self.mouse = [e.pageX, e.pageY];
        //self.handleClick(e);
    });
    $("#canvas").on('touchstart', function(e) {
        self.dragCoords = [e.originalEvent.touches[0].pageX - self.translateX, e.originalEvent.touches[0].pageY - self.translateY];
        self.isDragging = false;
        self.mouse = [e.originalEvent.touches[0].pageX, e.originalEvent.touches[0].pageY];
    });
    $("#canvas").mousemove(function(e) {
        if (self.isDragging) {
            self.translateX = e.pageX - self.dragCoords[0];
            self.translateY = e.pageY - self.dragCoords[1]
        }
    });
    $("#canvas").on('touchmove', function(e) {
        self.isDragging = true;
        e.preventDefault();
        self.translateX = e.originalEvent.touches[0].pageX - self.dragCoords[0];
        self.translateY = e.originalEvent.touches[0].pageY - self.dragCoords[1]
    });
    $("#canvas").mouseup(function(e) {
        self.isDragging = false;
        var newMouse = [e.pageX, e.pageY];
        if (Math.abs(newMouse[0] - self.mouse[0]) < 5 && Math.abs(newMouse[1] - self.mouse[1]) < 5){
            self.handleClick(e);
        }
    }); 
    $("#canvas").on('touchend', function(e) {
        self.isDragging = false;
        var newMouse = [e.changedTouches[0].pageX, e.changedTouches[0].pageX];
        if (!self.isDragging){
            self.handleClick(e);
        }
    });
    
    // Online
    this.socket = null;
    this.connecting = null;
};

Engine.prototype.start = function () {
    var self = this;
    
    window.requestAnimationFrame(function (time) {
        self.animate.call(self, time);
    });
};

Engine.prototype.animate = function (time) {
    var self = this;
    
    
    // If in a game, draw game
    // If new load, show party select screen
    if (this.connecting)
        this.animateConnecting(time, self);
    else if (this.battle)
        this.animateBattle(time, self);
    else
        this.animateMenu(time, self);
};

Engine.prototype.animateConnecting = function(time, self) {
    this.context.clearRect(0, 0, getWidth(), getHeight());
    this.context.fillStyle = "#000";
    this.context.textAlign = "center";
    this.context.fillText("Connecting..", getWidth()/2, getHeight()/2);
    
    // call next frame
    window.requestAnimationFrame(function(time) {
        self.animate.call(self, time);
    });    
};
Engine.prototype.animateMenu = function(time, self) {
    this.drawMenu();
    
    // call next frame
    window.requestAnimationFrame(function(time) {
        self.animate.call(self, time);
    });    
};

Engine.prototype.animateBattle = function(time, self) {
    // Update
    this.updateCharCoords();
    this.updateCurrentTurn();
    this.updateClickTarget();
    this.updateTileOverlay();
    this.checkDeaths();
    
    // Draw
    this.drawFrame();
    
    // call next frame
    window.requestAnimationFrame(function(time) {
        self.animate.call(self, time);
    });    
};

/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~  Updating Functions */
Engine.prototype.updateCharCoords = function () {
    for (var id in this.units)
    {
        var p = this.units[id];
        if (this.charMoving && p.isMyTurn) {
            // move towards the next point in the move path. If you're on it, delete it,
            // and move towards the next point. If there is no next point, char is done moving.
            // reset move mode and change tile (move CharTo)
            console.log(this.movePath);
            if (this.movePath.length > 0) {
                // move towards the next tile
                var dist = Math.sqrt(Math.pow(p.renderX - this.movePath[0].renderX, 2) + Math.pow(p.renderY - this.movePath[0].renderY,2))
                console.log(dist)
                if(dist > 6) {
                    if (this.movePath[0].x > p.x || this.movePath[0].y > p.y) {
                        // TODO: functionalize this
                        this.map[p.y][p.x].isSelected = false;
                        this.map[p.y][p.x].OccupiedBy = null;
                        p.x = this.movePath[0].x;
                        p.y = this.movePath[0].y;
                        this.map[p.y][p.x].isSelected = true;
                        this.map[p.y][p.x].OccupiedBy = p.id;
                    }
                    // move towards next target
                    if (p.renderX - this.movePath[0].renderX > 0)
                        p.renderX -=4;
                    else
                        p.renderX +=4;
                    // TODO: this needs to be 2 * the difference between the tile heights to eliminate jumpy movement
                    if (p.renderY - this.movePath[0].renderY > 0)
                        p.renderY -=2;
                    else
                        p.renderY +=2;
                    
                    p.facing = (p.renderX - this.movePath[0].renderX > 0) ? ((p.renderY - this.movePath[0].renderY > 0) ? 3 : 0) : ((p.renderY - this.movePath[0].renderY > 0) ? 2 : 1)
                } else {
                    p.updateRenderCoords(this.movePath[0]);   
                    this.movePath.splice(0, 1);
                }
            } else {
                // We are at the final point
                this.charMoving = false;
                this.map[p.y][p.x].isSelected = false;
                this.map[p.y][p.x].OccupiedBy = null;
                p.x = this.moveDest.x;
                p.y = this.moveDest.y;
                this.map[p.y][p.x].isSelected = true;
                this.map[p.y][p.x].OccupiedBy = p.id;
                p.hasMoved = true;
                p.isMoveMode = false;
                this.removeTileOverlay();
                this.moveDest = null;
            }
        } else {
            p.updateRenderCoords(this.map[p.y][p.x]);
        }
    }
};

Engine.prototype.updateCurrentTurn = function() {
    // If i directly say its not my turn, or indirectly say i'm done all my actions,
    // select next unit
    if (!this.units[this.currTurn].isMyTurn || !this.units[this.currTurn].isStillMyTurn() || this.units[this.currTurn].dead) {
        // select next unit
        this.units[this.currTurn].EndTurn();
        this.deselectUnit(this.map[this.units[this.currTurn].y][this.units[this.currTurn].x]);
        var newID = "unit" + (parseInt(this.currTurn.replace("unit", "")) + 1);
        if (!(newID in this.units)) { newID = "unit0"; }
        this.currTurn = newID;
        if (this.units[newID].dead) {
            this.updateCurrentTurn();
            return;
        }
        this.units[newID].StartTurn();
        console.log(this.units[newID].name + "'s turn.");
    }    
};
Engine.prototype.checkDeaths = function() {
    for (id in this.units) {
        if (!this.units[id].dead && this.units[id].hp <= 0) {
            var u = this.units[id];
            u.dead = true;
            this.map[u.y][u.x].OccupiedBy = null;
            console.log(u.name + " has died!");
            
            // Remove from their respective lists
            if ($.inArray(u.id, this.enemy) > 1) {
                this.enemy.splice($.inArray(u.id, this.enemy), 1);
            } else {
                this.player.splice($.inArray(u.id, this.enemy), 1);
            }
        };
    }
    // Check win / lose conditions and return to menu / matcher
    if (this.enemy.length == 0)
        ; // win
    if (this.player.length == 0)
        ; //lose
    // Also need to apply exp boost if it's a win, and need to save player progress from the battle to local storage /
    // server before returning to menu
};

Engine.prototype.updateTileOverlay = function() {
    var t1 = (this.selectedChar !== null) ? 
            this.map[this.units[this.selectedChar].y][this.units[this.selectedChar].x] : null,
        sC = this.units[this.selectedChar];
    for (var y = 0; y < this.map.length; y++)
      for(var x = 0; x < this.map[y].length; x++) {  
          var t2 = this.map[y][x];
          t2.isMoveOverlay = false;
          t2.isAttackOverlay = false;
          if(t1 !== null) {
              if (sC.isMoveMode) {
                  if (t1.distTo(t2) <= sC.move && this.FindPath(t1, t2, sC.jump).length <= sC.move && !t2.isSelected && t2.OccupiedBy === null)
                      t2.isMoveOverlay = true;
              } else if (sC.isSkillMode) {
                  if (t1.distTo(t2) <= sC.atkRange && !t2.isSelected)
                      t2.isAttackOverlay = true;
              }
          }
      }
};
Engine.prototype.removeTileOverlay = function() {
    for (var y = 0; y < this.map.length; y++)
      for(var x = 0; x < this.map[y].length; x++) {
          this.map[y][x].isMoveOverlay = false;
          this.map[y][x].isAttackOverlay = false;
      }
};

Engine.prototype.updateClickTarget = function () {
    var sC = this.units[this.selectedChar];
    if (sC === null || sC === undefined) {
        this.clicksTargetTiles = false;
        return;
    }
    this.clicksTargetTiles = ((sC.isMoveMode && !sC.hasMoved) || (sC.isSkillMode && !sC.hasActed));
};

/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Mechanics */
Engine.prototype.moveCharTo = function(x, y) {
    // 1. Remove selected tile
    // 2. Move char
    // 3. Reactivate selected tile in new location
    // 4. Char has already moved - remove overlay
    var mapRows = this.map;
    var sC = this.units[this.selectedChar];
    if (sC.isMyTurn) {
        mapRows[sC.y][sC.x].isSelected = false;
        mapRows[sC.y][sC.x].OccupiedBy = null;
        sC.x = x;
        sC.y = y;
        mapRows[sC.y][sC.x].isSelected = true;
        mapRows[sC.y][sC.x].OccupiedBy = sC.id;
        sC.hasMoved = true;
        sC.isMoveMode = false;
        this.removeTileOverlay();
    }
};

/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~  Drawing Functions */
Engine.prototype.drawFrame = function() {
    // Draw background
    
    var bg = this.context.createLinearGradient(0,0, 0, getHeight());
    bg.addColorStop(0, "#b5d9f0");
    bg.addColorStop(0.5, "#b4e3e9");
    bg.addColorStop(1, "#669eb8");
    
    this.context.fillStyle = bg;
    this.context.fillRect(0, 0, this.context.canvas.width, this.context.canvas.height);
    
    // Draw game map
    this.drawMap();
    
    // draw Turn Order
    this.drawTurnOrder();
    
    // draw Sidebar
    this.drawSideBar();    
}

Engine.prototype.drawMap = function () {
    var mapRows = this.map;
    for (var x = 0; x < mapRows.length; x++)
      for(var y = 0; y < mapRows[x].length; y++) {  
          var t = mapRows[x][y];
          this.drawTile(t);
          if (t.OccupiedBy !== null) {
              this.drawUnit(this.units[t.OccupiedBy], this.units[t.OccupiedBy].renderX + this.translateX, this.units[t.OccupiedBy].renderY + this.translateY);
          }
      }
};

Engine.prototype.drawTile = function (tile) {
    this.context.drawImage(Resources["tile" + tile.type], tile.renderX + this.translateX, tile.renderY + this.translateY);
    if (tile.isSelected)
        this.context.drawImage(Resources["tileSelected"], tile.renderX + this.translateX, tile.renderY + this.translateY);
    if (tile.isMoveOverlay || tile.isAttackOverlay)
        this.context.drawImage(Resources["tileOverlay"], tile.renderX + this.translateX, tile.renderY + this.translateY);
};

Engine.prototype.drawUnit = function (unit, x, y) {
    // TODO - find a way to draw that chars specific img
    if (unit.isMyTurn) { this.context.drawImage(Resources["turnIndicator" + (($.inArray(unit.id, this.enemy) > -1) ? "Enemy" : "")], x, y); }
    this.context.drawImage(Resources[unit.imgName + "Sprite"], 100*unit.facing, 0, 100, 100, x, y, 100, 100);
};

Engine.prototype.drawTurnOrder = function() {
    var x = 10;
    for (var u in this.units) {
        this.drawUnit(this.units[u], x, getHeight() - 110);
        x += 60;
    };
};

Engine.prototype.drawSideBar = function() {
    if (this.selectedChar !== null) {
        var sC = this.units[this.selectedChar];
        var bg = this.context.createLinearGradient(0, 0, 300, getHeight());
        bg.addColorStop(0, 'rgba(133,112,81, 0.9)');
        bg.addColorStop(1, 'rgba(113,91,74, 0.9)');
        this.context.fillStyle = bg;
        this.context.fillRect(getWidth() - 300, 0, getWidth(), getHeight());
        
        // Char Info
        this.context.font = '20pt Arial';
        this.context.fillStyle = '#fff';
        this.context.textAlign = "start";
        this.context.fillText(sC.name, getWidth() - 250, 50);
        
        // Text
        this.context.textAlign = "end";
        this.context.font = '12pt Arial';
        this.context.fillText(sC.hp + " / " + sC.maxHP, getWidth() - 50, 97);
        this.context.fillText(sC.mp + " / " + sC.maxMP, getWidth() - 50, 147);
        
        this.context.beginPath();
        this.context.rect(getWidth() - 250, 100, 200, 30);
        this.context.stroke();
        this.context.closePath();
        this.context.beginPath();
        this.context.fillStyle = "#c82124";
        this.context.fillRect(getWidth() - 250, 100, (sC.hp / sC.maxHP)*200, 30);
        this.context.closePath;
        
        this.context.beginPath();
        this.context.rect(getWidth() - 250, 150, 200, 30);
        this.context.stroke();
        this.context.closePath();
        this.context.beginPath();
        this.context.fillStyle = "#169AA1";
        this.context.fillRect(getWidth() - 250, 150, (sC.mp / sC.maxMP)*200, 30);
        this.context.closePath;
        

        // Controls
        if (this.units[this.selectedChar].isMyTurn) {
            
            if (this.socket == null || $.inArray(this.units[this.selectedChar].id, this.player) > -1) {
                // Attack
                this.context.beginPath();
                this.context.fillStyle = (sC.isSkillMode || sC.hasActed) ? "#666" : "#ccc";
                this.context.arc(getWidth() - 150, 250, 75, 0, 2 * Math.PI);
                this.context.fill();
                this.context.closePath();
                this.context.drawImage(Resources["attackUI"], getWidth() - 225, 175, 150, 150);
                // Undo Move
                this.context.beginPath();
                this.context.fillStyle = (sC.hasMoved && !sC.hasActed) ? "#82D141" : "#81A167";
                this.context.arc(getWidth() - 150, 400, 75, 0, 2 * Math.PI);
                this.context.fill();
                this.context.closePath();
                this.context.drawImage(Resources["undo"], getWidth() - 225, 325, 150, 150);
                // End Turn
                this.context.beginPath();
                this.context.fillStyle = "#d6593d";
                this.context.arc(getWidth() - 150, 550, 75, 0, 2 * Math.PI);
                this.context.fill();
                this.context.closePath();
                this.context.fillStyle = "#fff";
                this.context.textAlign = "center";
                this.context.fillText("End Turn", getWidth() - 150, 550);
            } else {
                this.context.fillStyle = "#fff";
                this.context.textAlign = "center";
                this.context.fillText("Waiting for other player..", getWidth() - 150, 300);
            }
        }
    }
};

/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Menu Functions */
Engine.prototype.drawMenu = function() {
    this.context.clearRect(0, 0, getWidth(), getHeight());
    
    this.context.beginPath();
    this.context.fillStyle = "#c82124";
    this.context.rect(getWidth()/3 - 100, getHeight()/2 - 25, 200, 50);
    this.context.fill();
    this.context.closePath();
    
    this.context.fillStyle = "#fff";
    this.context.textAlign = "center";
    this.context.font = '16pt Arial';
    this.context.fillText("Start Battle", getWidth()/3, getHeight()/2 + 5);
    
    this.context.beginPath();
    this.context.fillStyle = "#2190C8";
    this.context.rect(2*getWidth()/3 - 100, getHeight()/2 - 25, 200, 50);
    this.context.fill();
    this.context.closePath();
    
    this.context.fillStyle = "#fff";
    this.context.textAlign = "center";
    this.context.font = '16pt Arial';
    this.context.fillText("Start Online Battle", 2*getWidth()/3, getHeight()/2 + 5);
};
    
/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Init Functions */
Engine.prototype.generateMap = function(json) {
    // In online play, Server will pass the map to me
    if (json == undefined)
        return this.generateMapFromJSON('[[{"x":0,"y":0,"h":-4,"type":2,"walkable":true},{"x":1,"y":0,"h":1,"type":2,"walkable":true},{"x":2,"y":0,"h":1,"type":2,"walkable":true},{"x":3,"y":0,"h":1,"type":2,"walkable":true},{"x":4,"y":0,"h":-1,"type":2,"walkable":false},{"x":5,"y":0,"h":7,"type":3,"walkable":true},{"x":6,"y":0,"h":7,"type":3,"walkable":true},{"x":7,"y":0,"h":6,"type":4,"walkable":true},{"x":8,"y":0,"h":6,"type":0,"walkable":true},{"x":9,"y":0,"h":6,"type":0,"walkable":true},{"x":10,"y":0,"h":6,"type":0,"walkable":true},{"x":11,"y":0,"h":3,"type":2,"walkable":true},{"x":12,"y":0,"h":6,"type":2,"walkable":true},{"x":13,"y":0,"h":6,"type":2,"walkable":true},{"x":14,"y":0,"h":3,"type":2,"walkable":true},{"x":15,"y":0,"h":6,"type":0,"walkable":true},{"x":16,"y":0,"h":6,"type":0,"walkable":true},{"x":17,"y":0,"h":5,"type":1,"walkable":true}],[{"x":0,"y":1,"h":-4,"type":2,"walkable":true},{"x":1,"y":1,"h":1,"type":2,"walkable":true},{"x":2,"y":1,"h":1,"type":2,"walkable":true},{"x":3,"y":1,"h":1,"type":2,"walkable":true},{"x":4,"y":1,"h":0,"type":2,"walkable":true},{"x":5,"y":1,"h":7,"type":3,"walkable":true},{"x":6,"y":1,"h":7,"type":3,"walkable":true},{"x":7,"y":1,"h":6,"type":4,"walkable":true},{"x":8,"y":1,"h":6,"type":0,"walkable":true},{"x":9,"y":1,"h":6,"type":0,"walkable":true},{"x":10,"y":1,"h":6,"type":0,"walkable":true},{"x":11,"y":1,"h":6,"type":0,"walkable":true},{"x":12,"y":1,"h":6,"type":2,"walkable":true},{"x":13,"y":1,"h":6,"type":2,"walkable":true},{"x":14,"y":1,"h":6,"type":0,"walkable":true},{"x":15,"y":1,"h":6,"type":0,"walkable":true},{"x":16,"y":1,"h":6,"type":0,"walkable":true},{"x":17,"y":1,"h":5,"type":0,"walkable":true}],[{"x":0,"y":2,"h":-4,"type":2,"walkable":true},{"x":1,"y":2,"h":1,"type":2,"walkable":true},{"x":2,"y":2,"h":1,"type":2,"walkable":true},{"x":3,"y":2,"h":1,"type":2,"walkable":true},{"x":4,"y":2,"h":-1,"type":2,"walkable":false},{"x":5,"y":2,"h":7,"type":3,"walkable":true},{"x":6,"y":2,"h":7,"type":3,"walkable":true},{"x":7,"y":2,"h":6,"type":4,"walkable":true},{"x":8,"y":2,"h":6,"type":0,"walkable":true},{"x":9,"y":2,"h":6,"type":0,"walkable":true},{"x":10,"y":2,"h":6,"type":0,"walkable":true},{"x":11,"y":2,"h":6,"type":1,"walkable":true},{"x":12,"y":2,"h":6,"type":2,"walkable":true},{"x":13,"y":2,"h":6,"type":2,"walkable":true},{"x":14,"y":2,"h":6,"type":0,"walkable":true},{"x":15,"y":2,"h":6,"type":1,"walkable":true},{"x":16,"y":2,"h":6,"type":0,"walkable":true},{"x":17,"y":2,"h":5,"type":0,"walkable":true}],[{"x":0,"y":3,"h":-4,"type":2,"walkable":true},{"x":1,"y":3,"h":1,"type":2,"walkable":true},{"x":2,"y":3,"h":1,"type":2,"walkable":true},{"x":3,"y":3,"h":1,"type":2,"walkable":true},{"x":4,"y":3,"h":0,"type":2,"walkable":true},{"x":5,"y":3,"h":7,"type":3,"walkable":true},{"x":6,"y":3,"h":7,"type":3,"walkable":true},{"x":7,"y":3,"h":6,"type":4,"walkable":true},{"x":8,"y":3,"h":6,"type":0,"walkable":true},{"x":9,"y":3,"h":5,"type":0,"walkable":true},{"x":10,"y":3,"h":6,"type":0,"walkable":true},{"x":11,"y":3,"h":6,"type":0,"walkable":true},{"x":12,"y":3,"h":6,"type":2,"walkable":true},{"x":13,"y":3,"h":6,"type":2,"walkable":true},{"x":14,"y":3,"h":6,"type":0,"walkable":true},{"x":15,"y":3,"h":7,"type":0,"walkable":true},{"x":16,"y":3,"h":6,"type":0,"walkable":true},{"x":17,"y":3,"h":6,"type":0,"walkable":true}],[{"x":0,"y":4,"h":-4,"type":2,"walkable":true},{"x":1,"y":4,"h":1,"type":2,"walkable":true},{"x":2,"y":4,"h":1,"type":2,"walkable":true},{"x":3,"y":4,"h":1,"type":2,"walkable":true},{"x":4,"y":4,"h":-1,"type":2,"walkable":false},{"x":5,"y":4,"h":7,"type":3,"walkable":true},{"x":6,"y":4,"h":7,"type":3,"walkable":true},{"x":7,"y":4,"h":6,"type":4,"walkable":true},{"x":8,"y":4,"h":6,"type":0,"walkable":true},{"x":9,"y":4,"h":5,"type":0,"walkable":true},{"x":10,"y":4,"h":5,"type":0,"walkable":true},{"x":11,"y":4,"h":6,"type":0,"walkable":true},{"x":12,"y":4,"h":6,"type":2,"walkable":true},{"x":13,"y":4,"h":6,"type":2,"walkable":true},{"x":14,"y":4,"h":6,"type":0,"walkable":true},{"x":15,"y":4,"h":7,"type":0,"walkable":true},{"x":16,"y":4,"h":6,"type":0,"walkable":true},{"x":17,"y":4,"h":6,"type":0,"walkable":true}],[{"x":0,"y":5,"h":-4,"type":2,"walkable":true},{"x":1,"y":5,"h":1,"type":2,"walkable":true},{"x":2,"y":5,"h":1,"type":2,"walkable":true},{"x":3,"y":5,"h":1,"type":2,"walkable":true},{"x":4,"y":5,"h":0,"type":2,"walkable":true},{"x":5,"y":5,"h":7,"type":3,"walkable":true},{"x":6,"y":5,"h":7,"type":3,"walkable":true},{"x":7,"y":5,"h":6,"type":4,"walkable":true},{"x":8,"y":5,"h":6,"type":0,"walkable":true},{"x":9,"y":5,"h":5,"type":0,"walkable":true},{"x":10,"y":5,"h":6,"type":0,"walkable":true},{"x":11,"y":5,"h":6,"type":0,"walkable":true},{"x":12,"y":5,"h":6,"type":2,"walkable":true},{"x":13,"y":5,"h":6,"type":2,"walkable":true},{"x":14,"y":5,"h":6,"type":0,"walkable":true},{"x":15,"y":5,"h":6,"type":0,"walkable":true},{"x":16,"y":5,"h":6,"type":0,"walkable":true},{"x":17,"y":5,"h":6,"type":1,"walkable":true}],[{"x":0,"y":6,"h":0,"type":2,"walkable":true},{"x":1,"y":6,"h":1,"type":2,"walkable":true},{"x":2,"y":6,"h":1,"type":2,"walkable":true},{"x":3,"y":6,"h":1,"type":2,"walkable":true},{"x":4,"y":6,"h":-1,"type":2,"walkable":false},{"x":5,"y":6,"h":7,"type":3,"walkable":true},{"x":6,"y":6,"h":7,"type":3,"walkable":true},{"x":7,"y":6,"h":6,"type":4,"walkable":true},{"x":8,"y":6,"h":6,"type":0,"walkable":true},{"x":9,"y":6,"h":6,"type":0,"walkable":true},{"x":10,"y":6,"h":6,"type":0,"walkable":true},{"x":11,"y":6,"h":6,"type":0,"walkable":true},{"x":12,"y":6,"h":6,"type":2,"walkable":true},{"x":13,"y":6,"h":6,"type":2,"walkable":true},{"x":14,"y":6,"h":6,"type":0,"walkable":true},{"x":15,"y":6,"h":6,"type":1,"walkable":true},{"x":16,"y":6,"h":6,"type":0,"walkable":true},{"x":17,"y":6,"h":6,"type":1,"walkable":true}],[{"x":0,"y":7,"h":0,"type":2,"walkable":true},{"x":1,"y":7,"h":1,"type":2,"walkable":true},{"x":2,"y":7,"h":1,"type":2,"walkable":true},{"x":3,"y":7,"h":1,"type":2,"walkable":true},{"x":4,"y":7,"h":1,"type":2,"walkable":true},{"x":5,"y":7,"h":2,"type":2,"walkable":true},{"x":6,"y":7,"h":7,"type":3,"walkable":true},{"x":7,"y":7,"h":6,"type":4,"walkable":true},{"x":8,"y":7,"h":6,"type":0,"walkable":true},{"x":9,"y":7,"h":6,"type":0,"walkable":true},{"x":10,"y":7,"h":6,"type":0,"walkable":true},{"x":11,"y":7,"h":6,"type":0,"walkable":true},{"x":12,"y":7,"h":6,"type":2,"walkable":true},{"x":13,"y":7,"h":6,"type":2,"walkable":true},{"x":14,"y":7,"h":6,"type":0,"walkable":true},{"x":15,"y":7,"h":6,"type":1,"walkable":true},{"x":16,"y":7,"h":6,"type":0,"walkable":true},{"x":17,"y":7,"h":6,"type":0,"walkable":true}],[{"x":0,"y":8,"h":-4,"type":2,"walkable":true},{"x":1,"y":8,"h":1,"type":2,"walkable":true},{"x":2,"y":8,"h":1,"type":2,"walkable":true},{"x":3,"y":8,"h":1,"type":2,"walkable":true},{"x":4,"y":8,"h":-1,"type":2,"walkable":false},{"x":5,"y":8,"h":2,"type":2,"walkable":true},{"x":6,"y":8,"h":7,"type":3,"walkable":true},{"x":7,"y":8,"h":6,"type":4,"walkable":true},{"x":8,"y":8,"h":6,"type":0,"walkable":true},{"x":9,"y":8,"h":7,"type":0,"walkable":true},{"x":10,"y":8,"h":7,"type":0,"walkable":true},{"x":11,"y":8,"h":6,"type":0,"walkable":true},{"x":12,"y":8,"h":6,"type":2,"walkable":true},{"x":13,"y":8,"h":6,"type":2,"walkable":true},{"x":14,"y":8,"h":6,"type":0,"walkable":true},{"x":15,"y":8,"h":6,"type":0,"walkable":true},{"x":16,"y":8,"h":6,"type":0,"walkable":true},{"x":17,"y":8,"h":6,"type":0,"walkable":true}],[{"x":0,"y":9,"h":-4,"type":2,"walkable":true},{"x":1,"y":9,"h":1,"type":2,"walkable":true},{"x":2,"y":9,"h":1,"type":2,"walkable":true},{"x":3,"y":9,"h":1,"type":2,"walkable":true},{"x":4,"y":9,"h":0,"type":2,"walkable":true},{"x":5,"y":9,"h":3,"type":2,"walkable":true},{"x":6,"y":9,"h":7,"type":3,"walkable":true},{"x":7,"y":9,"h":6,"type":4,"walkable":true},{"x":8,"y":9,"h":6,"type":0,"walkable":true},{"x":9,"y":9,"h":6,"type":0,"walkable":true},{"x":10,"y":9,"h":6,"type":0,"walkable":true},{"x":11,"y":9,"h":6,"type":0,"walkable":true},{"x":12,"y":9,"h":6,"type":2,"walkable":true},{"x":13,"y":9,"h":6,"type":2,"walkable":true},{"x":14,"y":9,"h":6,"type":0,"walkable":true},{"x":15,"y":9,"h":6,"type":1,"walkable":true},{"x":16,"y":9,"h":6,"type":0,"walkable":true},{"x":17,"y":9,"h":6,"type":1,"walkable":true}],[{"x":0,"y":10,"h":-4,"type":2,"walkable":true},{"x":1,"y":10,"h":1,"type":2,"walkable":true},{"x":2,"y":10,"h":1,"type":2,"walkable":true},{"x":3,"y":10,"h":1,"type":2,"walkable":true},{"x":4,"y":10,"h":-1,"type":2,"walkable":false},{"x":5,"y":10,"h":3,"type":2,"walkable":true},{"x":6,"y":10,"h":7,"type":3,"walkable":true},{"x":7,"y":10,"h":6,"type":4,"walkable":true},{"x":8,"y":10,"h":3,"type":2,"walkable":true},{"x":9,"y":10,"h":6,"type":0,"walkable":true},{"x":10,"y":10,"h":6,"type":0,"walkable":true},{"x":11,"y":10,"h":3,"type":2,"walkable":true},{"x":12,"y":10,"h":6,"type":2,"walkable":true},{"x":13,"y":10,"h":6,"type":2,"walkable":true},{"x":14,"y":10,"h":3,"type":2,"walkable":true},{"x":15,"y":10,"h":6,"type":1,"walkable":true},{"x":16,"y":10,"h":6,"type":0,"walkable":true},{"x":17,"y":10,"h":6,"type":1,"walkable":true}],[{"x":0,"y":11,"h":-4,"type":2,"walkable":true},{"x":1,"y":11,"h":1,"type":2,"walkable":true},{"x":2,"y":11,"h":1,"type":2,"walkable":true},{"x":3,"y":11,"h":1,"type":2,"walkable":true},{"x":4,"y":11,"h":0,"type":2,"walkable":true},{"x":5,"y":11,"h":4,"type":2,"walkable":true},{"x":6,"y":11,"h":5,"type":2,"walkable":true},{"x":7,"y":11,"h":6,"type":2,"walkable":true},{"x":8,"y":11,"h":6,"type":2,"walkable":true},{"x":9,"y":11,"h":6,"type":2,"walkable":true},{"x":10,"y":11,"h":6,"type":2,"walkable":true},{"x":11,"y":11,"h":6,"type":2,"walkable":true},{"x":12,"y":11,"h":6,"type":2,"walkable":true},{"x":13,"y":11,"h":6,"type":2,"walkable":true},{"x":14,"y":11,"h":6,"type":0,"walkable":true},{"x":15,"y":11,"h":6,"type":1,"walkable":true},{"x":16,"y":11,"h":6,"type":1,"walkable":true},{"x":17,"y":11,"h":6,"type":1,"walkable":true}],[{"x":0,"y":12,"h":-4,"type":2,"walkable":true},{"x":1,"y":12,"h":1,"type":2,"walkable":true},{"x":2,"y":12,"h":1,"type":2,"walkable":true},{"x":3,"y":12,"h":1,"type":2,"walkable":true},{"x":4,"y":12,"h":-1,"type":2,"walkable":false},{"x":5,"y":12,"h":4,"type":2,"walkable":true},{"x":6,"y":12,"h":5,"type":2,"walkable":true},{"x":7,"y":12,"h":6,"type":2,"walkable":true},{"x":8,"y":12,"h":6,"type":2,"walkable":true},{"x":9,"y":12,"h":6,"type":2,"walkable":true},{"x":10,"y":12,"h":6,"type":2,"walkable":true},{"x":11,"y":12,"h":6,"type":2,"walkable":true},{"x":12,"y":12,"h":6,"type":2,"walkable":true},{"x":13,"y":12,"h":6,"type":2,"walkable":true},{"x":14,"y":12,"h":6,"type":1,"walkable":true},{"x":15,"y":12,"h":6,"type":1,"walkable":true},{"x":16,"y":12,"h":5,"type":1,"walkable":true},{"x":17,"y":12,"h":6,"type":1,"walkable":true}],[{"x":0,"y":13,"h":-4,"type":2,"walkable":true},{"x":1,"y":13,"h":1,"type":2,"walkable":true},{"x":2,"y":13,"h":1,"type":2,"walkable":true},{"x":3,"y":13,"h":1,"type":2,"walkable":true},{"x":4,"y":13,"h":0,"type":2,"walkable":true},{"x":5,"y":13,"h":7,"type":3,"walkable":true},{"x":6,"y":13,"h":7,"type":3,"walkable":true},{"x":7,"y":13,"h":6,"type":4,"walkable":true},{"x":8,"y":13,"h":3,"type":2,"walkable":true},{"x":9,"y":13,"h":6,"type":1,"walkable":true},{"x":10,"y":13,"h":6,"type":1,"walkable":true},{"x":11,"y":13,"h":3,"type":2,"walkable":true},{"x":12,"y":13,"h":6,"type":1,"walkable":true},{"x":13,"y":13,"h":6,"type":1,"walkable":true},{"x":14,"y":13,"h":3,"type":2,"walkable":true},{"x":15,"y":13,"h":6,"type":1,"walkable":true},{"x":16,"y":13,"h":5,"type":1,"walkable":true},{"x":17,"y":13,"h":7,"type":1,"walkable":true}],[{"x":0,"y":14,"h":-4,"type":2,"walkable":true},{"x":1,"y":14,"h":1,"type":2,"walkable":true},{"x":2,"y":14,"h":1,"type":2,"walkable":true},{"x":3,"y":14,"h":1,"type":2,"walkable":true},{"x":4,"y":14,"h":-1,"type":2,"walkable":false},{"x":5,"y":14,"h":7,"type":3,"walkable":true},{"x":6,"y":14,"h":7,"type":3,"walkable":true},{"x":7,"y":14,"h":6,"type":4,"walkable":true},{"x":8,"y":14,"h":6,"type":1,"walkable":true},{"x":9,"y":14,"h":6,"type":1,"walkable":true},{"x":10,"y":14,"h":6,"type":1,"walkable":true},{"x":11,"y":14,"h":6,"type":1,"walkable":true},{"x":12,"y":14,"h":6,"type":1,"walkable":true},{"x":13,"y":14,"h":6,"type":1,"walkable":true},{"x":14,"y":14,"h":6,"type":1,"walkable":true},{"x":15,"y":14,"h":6,"type":1,"walkable":true},{"x":16,"y":14,"h":7,"type":1,"walkable":true},{"x":17,"y":14,"h":7,"type":1,"walkable":true}]]');
    else
        return this.generateMapFromJSON(json);                
};
Engine.prototype.generateMapFromJSON = function(JSONString) {
    var mapFile;
    var map =[];
    try {
        mapFile = JSON.parse(JSONString);
        this.map = [];
        var mapFileRow = [],
            mapRow = [];
        for (var y = 0; y < mapFile.length; y ++) {
            mapFileRow = mapFile[y];
            mapRow = [];
            for (var x = 0; x < mapFileRow.length; x++) {
                mapRow.push(new Tile(mapFileRow[x].x, mapFileRow[x].y, mapFile.length, mapFileRow[x].h, mapFileRow[x].type, mapFileRow[x].walkable));
            }
            map.push(mapRow);
        }
    } catch (e) {
        
    }
    return map;    
};
Engine.prototype.generatePlayerParty = function() {
    this.addPlayer(new Unit("Bob", 13, 0, 0, null, "player", 0));
    this.addPlayer(new Knight("Jim", 13, 1, 1, null, "player", 0));
    this.addPlayer(new Witch("Sally", 12, 0, 2, null, "player", 0));
    
    this.addEnemy(new Test("Test1", 1, 7, 3, null, "enemy", 1));
    this.addEnemy(new Test("Test2", 2, 6, 4, null, "enemy", 1));
    this.addEnemy(new Test("Test3", 2, 8, 5, null, "enemy", 1));
};
Engine.prototype.addPlayer = function(unit) {
    this.player.push(unit.id);
    this.addUnit(unit)
}
Engine.prototype.addEnemy = function(unit) {
    this.enemy.push(unit.id);
    this.addUnit(unit)
}
Engine.prototype.addUnit = function(unit) {
    this.units[unit.id] = unit;
    this.map[unit.y][unit.x].OccupiedBy = unit.id;
};

Engine.prototype.handleClick = function(e) {
    
    // Detect different click areas if we are in a battle vs in a menu
    if (this.battle) {
        // Is it a sidebar click?
        if(this.selectedChar !== null && e.pageX > (getWidth() - 300)) {
            this.handleSideBarClick(e);
            return;
        }    
        
        if (this.clicksTargetTiles) {
            this.findTileClick(e);
        } else {
            this.findUnitClick(e);
        }
    } else {
        // Offline Battle Start
        if (e.pageX > getWidth()/3 - 100 && e.pageX < getWidth()/3 + 100 && e.pageY > getHeight()/2 - 25 && e.pageY < getHeight()/2 + 25) {
            this.map = this.generateMap();
            this.generatePlayerParty();
            this.battle = true;
        } else if (e.pageX > 2*getWidth()/3 - 100 && e.pageX < 2*getWidth()/3 + 100 && e.pageY > getHeight()/2 - 25 && e.pageY < getHeight()/2 + 25) {
            // Initialize Socket;
            this.socket = new Socket('http://directedstudy-bdot.rhcloud.com:8000', this);
            // Wait for server handshake - separate function will be invoked
        }
    }
};

Engine.prototype.findTileClick = function(e) {
    var mapRows = this.map;
    var a = this.units[this.selectedChar]
    for (var y = 0; y < mapRows.length; y++)
      for(var x = 0; x < mapRows[y].length; x++) {  
          var targetTile = mapRows[y][x];
          // Only tiles with overlay are valid clicks for movement purposes
          if (targetTile.CheckPoint(e.pageX - this.translateX, e.pageY - this.translateY) && targetTile.isClickable()) {
            console.log("click on tile: " + targetTile.x + ", " + targetTile.y);
            
            if (targetTile.isSelected) {
                // deselect char
                this.deselectUnit(targetTile);
            } else if (targetTile.isMoveOverlay) {
                // Move char here:
                if (this.socket == null || $.inArray(this.units[this.selectedChar].id, this.player) > -1) {
                    a.oldX = a.x;
                    a.oldY = a.y;
                    this.charMoving = true;
                    var startTile = this.map[a.y][a.x];
                    this.movePath = this.FindPath(startTile, targetTile, a.jump);
                    this.moveDest = this.movePath[this.movePath.length - 1];
                    // Send to socket if online
                    if (this.socket != null) {
                        var data = {};
                        data.id = this.units[this.selectedChar].id;
                        data.path = this.movePath;
                        data.dest = this.moveDest;
                        this.socket.sendmsg('charMove', data);
                    }
                }
            } else if (targetTile.isAttackOverlay) {
                // Attack handler - determine if target exists,
                // who they are, and perform an attack action on them.
                if (targetTile.OccupiedBy !== null) {
                    this.executeAttack(a, targetTile);
                    // send command to server
                    if (this.socket != null) {
                        this.socket.sendmsg('attack', {attacker: a.id, x: x, y: y});
                    }
                }
            }
            break;
          }
      }  
};

Engine.prototype.executeAttack = function(attacker, targetTile) {
    console.log(attacker.name + " attacks " + this.units[targetTile.OccupiedBy].name);
    attacker.Attack(this.units[targetTile.OccupiedBy]);
    // change attacker facing to match attack-ee
    attacker.facing = (attacker.x - targetTile.x > 0) ? ((attacker.y - targetTile.y > 0) ? 3 : 0) : ((attacker.y - targetTile.y > 0) ? 2 : 1)
};
Engine.prototype.findUnitClick = function(e) {
    var charMatches = [],
        clickedChar;
    for (var id in this.units)
        if(!this.units[id].dead && this.units[id].CheckPoint(e.pageX - this.translateX, e.pageY - this.translateY)) {
            //console.log("Click on char: " + this.player[i].name);
            //break;
            charMatches.push(id);
        }
    if (charMatches.length > 1)
    {
        clickedChar = charMatches[0];
        for (var k = 1; k < charMatches.length; k++)
        {
            if (this.units[charMatches[k]].renderY > this.units[clickedChar].renderY)
                clickedChar = charMatches[k];
        }
    } else if (charMatches.length === 1) {
        clickedChar = charMatches[0];
    } else { return; }
    var isSelect = !this.map[this.units[clickedChar].y][this.units[clickedChar].x].isSelected
    // If there is another selected character, unselect it in the graphics
    if (isSelect && this.selectedChar !== null) {
        this.map[this.units[this.selectedChar].y][this.units[this.selectedChar].x].isSelected = false;   
    }
    // Toggle map tile selection
    this.map[this.units[clickedChar].y][this.units[clickedChar].x].isSelected = isSelect;
    this.selectedChar = (isSelect) ? clickedChar : null;

    console.log("Click on char: " + this.units[clickedChar].name);    
};

Engine.prototype.handleSideBarClick = function (e) {
    // TODO - make controls variables which store their coords and radius / square / handler?
    // So I don't have to remember for coding here - can just do a for loop iterating
    // through controls
    var sC = this.units[this.selectedChar];
    if (Math.sqrt(Math.pow(e.pageX - (getWidth() - 150), 2) + Math.pow(e.pageY - 250, 2)) <= 75) {
        // Attack mode toggle
        if (!sC.hasActed) {
            console.log("toggling attack mode");
            sC.isMoveMode = !(sC.isMoveMode || sC.hasMoved);
            sC.isSkillMode = !(sC.isSkillMode || sC.hasActed);
        }
    } else if (Math.sqrt(Math.pow(e.pageX - (getWidth() - 150), 2) + Math.pow(e.pageY - 400, 2)) <= 75) {
        // Undo unit's move
        if (sC.hasMoved && !sC.hasActed) {
            this.undoMove(sC);
            if (this.socket != null)
                this.socket.sendmsg('undoCharMove', { id: sC.id });

        }
    } else if (Math.sqrt(Math.pow(e.pageX - (getWidth() - 150), 2) + Math.pow(e.pageY - 550, 2)) <= 75) {
        // End Unit's turn
        console.log("Ending Turn");
        sC.EndTurn();
        this.deselectUnit(this.map[sC.y][sC.x]);
        
        if (this.socket != null)
            this.socket.sendmsg('endTurn', { id: sC.id });
    }
};

Engine.prototype.undoMove = function(unit) {
    console.log("Undoing move");
    unit.hasMoved = false;
    unit.isMoveMode = true;
    unit.isSkillMove = false;
    // TODO: functionalize this
    this.map[unit.y][unit.x].isSelected = false;
    this.map[unit.y][unit.x].OccupiedBy = null;
    unit.x = unit.oldX;
    unit.y = unit.oldY;
    this.map[unit.y][unit.x].isSelected = true;
    this.map[unit.y][unit.x].OccupiedBy = unit.id;
}

/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~  Path Finding - A* Method */
Engine.prototype.FindPath = function(startTile, endTile, maxH) {
    var openList = [],
        closedList = [],
        path = [];
        map = this.map;
    var tile = startTile;
    tile.G = 0;
    openList.push(tile);
    while (tile.x != endTile.x || tile.y != endTile.y) {
        if (typeof startTile == "undefined" || typeof endTile == "underfined") {
            break;
        }
        
        // Add all adjacent walkable squares to open list
        // make sure they all exist
        if(typeof map[tile.y+1] !== "undefined" && (Math.abs(map[tile.y+1][tile.x].h - tile.h) <= maxH) && $.inArray(closedList, map[tile.y+1][tile.x]) == -1) {
            if (map[tile.y+1][tile.x].isWalkable()) {
                openList.push(map[tile.y+1][tile.x]);
            }
        }
        if(typeof map[tile.y-1] !== "undefined" && (Math.abs(map[tile.y-1][tile.x].h - tile.h) <= maxH) && $.inArray(closedList, map[tile.y-1][tile.x]) == -1) {
            if (map[tile.y-1][tile.x].isWalkable()) {
                openList.push(map[tile.y-1][tile.x]);
            }
            
        }
        if(typeof map[tile.y][tile.x-1] !== "undefined" && (Math.abs(map[tile.y][tile.x-1].h - tile.h) <= maxH) && $.inArray(closedList, map[tile.y][tile.x-1]) == -1) {
            if (map[tile.y][tile.x-1].isWalkable()) {
                openList.push(map[tile.y][tile.x-1]);
            }
        }
        if(typeof map[tile.y][tile.x+1] !== "undefined" && (Math.abs(map[tile.y][tile.x+1].h - tile.h) <= maxH) && $.inArray(closedList, map[tile.y][tile.x+1]) == -1) {
            if (map[tile.y][tile.x+1].isWalkable()) {
                openList.push(map[tile.y][tile.x+1]);
            }
        }
        
        // set Previous tile as parent of all tiles
        //path.push(tile);
        // Remove parent from open list and add to closed
        for (var j=0; j < openList.length; j++) {
            if (openList[j].x == tile.x && openList[j].y == tile.y) {
                openList.splice(j, 1);
                closedList.push(tile);
            }
        }
        
        var minF = 100;
        var minFIndex = 0;
        for (var i=0; i < openList.length; i++) {
            // Pick square with lowest F and set as Tile
            // F = G + H
            // G = movement from start or G + 1
            // H = est movement cost to end tile
            var oL = openList[i];
            oL.G = tile.G + 1;
            oL.F = openList[i].G + (Math.abs(endTile.x - oL.x) + Math.abs(endTile.y - oL.y));
            if (oL.F < minF) {
                minF = oL.F;
                minFIndex = i;
            }
        }
        tile = openList[minFIndex];
        path.push(openList[minFIndex]);
        openList.splice(minFIndex, 1);
        
        if (openList.length > 200 || closedList.length > 200) {
            // Something has gone wrong. I am not sure what yet, but this shouldn't ever happen.
            break;
        }
    }
    
    return path;
};    
    

/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~  Helper Functions */

Engine.prototype.deselectUnit = function(tile) {
    this.removeTileOverlay();  
    if (this.selectedChar === null) return;
    this.units[this.selectedChar].isMoveMode = true;
    this.units[this.selectedChar].isAttackMode = false;
    this.selectedChar = null;
    tile.isSelected = false;
};
Engine.prototype.getObjectLength = function(obj) {
    var size = 0, key;
    for (key in obj) {
        if(obj.hasOwnProperty(key)) size++;
    }
    return size;
};

function getWidth() {
    if (self.innerWidth) {
       return self.innerWidth;
    }
    else if (document.documentElement && document.documentElement.clientWidth){
        return document.documentElement.clientWidth;
    }
    else if (document.body) {
        return document.body.clientWidth;
    }
    return 0;
}
function getHeight() {
    if (self.innerHeight) {
       return self.innerHeight;
    }
    else if (document.documentElement && document.documentElement.clientHeight){
        return document.documentElement.clientHeight;
    }
    else if (document.body) {
        return document.body.clientHeight;
    }
    return 0;
}