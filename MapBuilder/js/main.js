var Resources;
var MapBuilder = function(canvasID) {
    var self = this,
        canvas = document.getElementById(canvasID);
    canvas.width = getWidth();
    canvas.height = getHeight();
    this.context = canvas.getContext('2d');
    this.map = [[new Tile(0,0,1,1,1, true)]];
    
    this.selected = [];
    
    this.mouseDown = false;
    this.mouse = null;
    
    // Event handlers
    $("#numX, #numY").change(function(e) {
        self.generateMap();
    });
    /*
    $("canvas").click(function(e) {
        if (!this.mouseMoved)
            self.handleClick(e);
    });*/
    $("canvas").mousedown(function(e) {
        this.mouse = [e.pageX, e.pageY];
        this.mouseDown = true;
    });
    $("canvas").mouseup(function(e) {
        var newMouse = [e.pageX, e.pageY];
        if (Math.abs(newMouse[0] - this.mouse[0]) < 5 && Math.abs(newMouse[1] - this.mouse[1]) < 5){
            self.handleClick(e);
        }
        this.mouseDown = false;
    });
    $("canvas").mousemove(function(e) {
        if (this.mouseDown) {
            self.handleMouseMove(e);
        }
    });
};


MapBuilder.prototype.Draw = function() {
    this.context.clearRect(0, 0, this.context.canvas.width, this.context.canvas.height);
    for (var y=0; y < this.map.length; y++) 
        for (var x=0; x < this.map[y].length; x++)
            this.drawTile(this.map[y][x]);
    
    // Draw interface
    this.context.beginPath;
    this.context.fillStyle = "#c82124";
    this.context.arc(getWidth() - 100, 100, 50, 0, 2*Math.PI);
    this.context.fill();
    this.context.closePath();
    this.context.beginPath();
    this.context.fillStyle = "#1c6eba";
    this.context.arc(getWidth() - 100, 220, 50, 0, 2*Math.PI);
    this.context.fill()
    this.context.closePath();
    
    this.context.beginPath();
    this.context.fillStyle = "#c82124";
    this.context.arc(getWidth() - 200, 50, 30, 0, 2*Math.PI);
    this.context.fill();
    this.context.closePath();
    this.context.beginPath();
    this.context.fillStyle = "#1c6eba";
    this.context.arc(getWidth() - 350, 50, 30, 0, 2*Math.PI);
    this.context.fill()
    this.context.closePath();
    
    this.context.beginPath();
    this.context.fillStyle = '#334455';
    this.context.fillRect(5, 0, 150, 25);
    this.context.closePath();
    this.context.beginPath();
    this.context.fillStyle = '#bbb';
    this.context.fillRect(5, 100, 150, 25);
    this.context.closePath();
    
    this.context.beginPath();
    this.context.fillStyle = '#8CE324';
    this.context.arc(getWidth() - 250, 200, 30, 0, 2*Math.PI);
    this.context.fill();
    this.context.closePath();
    
    // UI Text
    this.context.fillStyle = "#fff";
    this.context.font = '12pt Arial';
    this.context.textAlign = 'center';
    this.context.fillText("Load from JSON", 75, 12);
    this.context.fillText("Save to Console", 75, 112);
    this.context.fillStyle = "#000";
    this.context.fillText("x", 165, 45);
    this.context.fillText("y", 165, 85);
            
};
MapBuilder.prototype.drawTile = function (tile) {
    this.context.drawImage(Resources["tile" + tile.type], tile.renderX, tile.renderY);
    if (tile.isSelected)
        this.context.drawImage(Resources["tileSelected"], tile.renderX, tile.renderY);
    if (!tile.isWalkable())
        this.context.drawImage(Resources["tileNoWalk"], tile.renderX, tile.renderY);
};

MapBuilder.prototype.generateMap = function() {
    var vx = parseInt($("#numX").val()),
        vy = parseInt($("#numY").val());
    //this.map = [];
    var mapRow = [];
    var x, y;
    if (vy > this.map.length) {
        for (y = 0; y < this.map.length; y++) {
            mapRow = this.map[y];
            for (x = 0; x < mapRow.length; x++) {
                var oldT = mapRow[x];
                mapRow[x] = (new Tile(oldT.x,oldT.y, vy, oldT.h, oldT.type, true));   
            }
        }
        for (y = this.map.length; y < vy; y++) {
            mapRow = [];
            for(x=0; x < vx; x++) {
                mapRow.push(new Tile(x, y, vy, 1, 1, true));
            }
            this.map.push(mapRow);
        }
    } else if (vx > this.map[0].length) {
        for (y = 0; y < this.map.length; y++) {
            mapRow = this.map[y];
            for (x = 0; x < mapRow.length; x++) {
                var oldT = mapRow[x];
                mapRow[x] = (new Tile(oldT.x,oldT.y, vy, oldT.h, oldT.type, true));   
            }
        }
        for (y = 0; y < vy; y++) {
            mapRow = this.map[y];
            for (x = mapRow.length; x < vx; x++) {
                mapRow.push(new Tile(x,y,vy,1,1, true));   
            }
        }
    } else if (vy < this.map.length) {
        for (y = this.map.length - 1; y >= vy; y--) {
            this.map.pop();   
        }
    } else if (vx < this.map[0].length) {
        for (y = 0; y < vy; y++) {
            mapRow = this.map[y];
            for (x = mapRow.length - 1; x >= vx; x-- ) {
                mapRow.pop();   
            }
        }
    }
    
    this.Draw();
}

MapBuilder.prototype.handleClick = function(e) {
    // Check interface buttons
    var i,
        c = false;
    if (Math.sqrt(Math.pow(e.pageX - (getWidth() - 100), 2) + Math.pow(e.pageY - 100, 2)) <= 50) {
        // Up Arrow
        console.log("Up Arrow Pressed");
        for (i=0; i < this.selected.length; i++) {
            this.selected[i].RaiseHeight();
            c=true;
        }
    
    } else if (Math.sqrt(Math.pow(e.pageX - (getWidth() - 100), 2) + Math.pow(e.pageY - 220, 2)) <= 50) {
        // Down Arrow
        console.log("Down Arrow Pressed");
        for (i=0; i < this.selected.length; i++) {
            this.selected[i].LowerHeight();
            c=true;
        }
    
    } else if (Math.sqrt(Math.pow(e.pageX - (getWidth() - 200), 2) + Math.pow(e.pageY - 50, 2)) <= 30) {
        for (i=0; i < this.selected.length; i++) {
            this.selected[i].type = this.getNextTileTexture(this.selected[i].type);
            c=true;
        }  
    } else if (Math.sqrt(Math.pow(e.pageX - (getWidth() - 350), 2) + Math.pow(e.pageY - 50, 2)) <= 30) {
        for (i=0; i < this.selected.length; i++) {
            this.selected[i].type = this.getPrevTileTexture(this.selected[i].type);
            c=true;
            
        }  
    } else if (e.pageX > 10 && e.pageX < 160 && e.pageY > 100 && e.pageY < 130) {
        c = true;
        // save
        this.saveMap();
    } else if (e.pageX > 10 && e.pageX < 160 && e.pageY > 0 && e.pageY < 30) {
        c = true;
        $("#loadDialog").removeClass("hidden");
    } else if (Math.sqrt(Math.pow(e.pageX - (getWidth() - 250), 2) + Math.pow(e.pageY - 200, 2)) <= 30) {
        // toggle walkable
        for (i=0; i < this.selected.length; i++) {
            this.selected[i].walkable = !this.selected[i].walkable;
            c = true;
        }
    }
    
    
    if (c) {
        this.Draw();
        return;
    }
    
    // Check Map Tiles
    var mapRows = this.map;
    yLoop:
    for (var y = mapRows.length - 1; y >= 0 ; y--)
      xLoop:
      for(var x = mapRows[y].length - 1; x >= 0; x--) {  
          var targetTile = mapRows[y][x];
          if (targetTile.CheckPoint(e.pageX, e.pageY)) {
            console.log("click on tile: " + targetTile.x + ", " + targetTile.y);
            targetTile.isSelected = !targetTile.isSelected;
            if (targetTile.isSelected) { this.selected.push(targetTile); }
            else if ($.inArray(targetTile, this.selected) > -1) {
                this.selected.splice($.inArray(targetTile, this.selected), 1);
            }
            c=true;
            break yLoop;
          }
      }
    if (!c) {
        // Click is outside all inputs / tiles - Unselect all tiles here
        for (i = 0; i < this.selected.length; i++) {
            this.selected[i].isSelected = false;   
        }
        this.selected = [];
    }
    // TODO
    this.Draw();
};
MapBuilder.prototype.handleMouseMove = function(e) {
    // Check Map Tiles
    var mapRows = this.map;
    yLoop:
    for (var y = mapRows.length - 1; y >= 0 ; y--)
      xLoop:
      for(var x = mapRows[y].length - 1; x >= 0; x--) {  
          var targetTile = mapRows[y][x];
          if (targetTile.CheckPoint(e.pageX, e.pageY)) {
            if (!targetTile.isSelected) {
                console.log("tile selected at: " + targetTile.x + ", " + targetTile.y);
                targetTile.isSelected = true;
                if (targetTile.isSelected) { this.selected.push(targetTile); }
                this.Draw();
            }
            break yLoop;
          }
      }
};

MapBuilder.prototype.getNextTileTexture = function(i) {
    i = i + 1;
    if (i >= Resources.numTileImg) { i = 0; }
    return i;
};
MapBuilder.prototype.getPrevTileTexture = function(i) {
    i = i - 1;
    if (i < 0) { i = Resources.numTileImg - 1; }
    return i;
};

MapBuilder.prototype.saveMap = function () {
    var strippedMap = [],
        strippedRow = [],
        mapRow = [],
        sT = new Object();
    for (var y = 0; y < this.map.length; y++) {
        strippedRow = [];
        mapRow = this.map[y];
        for (var x = 0; x < mapRow.length; x++) {
            var t = mapRow[x];
            sT = new Object();
            sT.x = t.x;
            sT.y = t.y;
            sT.h = t.h;
            sT.type = t.type;
            sT.walkable = t.walkable;
            strippedRow.push(sT);
        }
        strippedMap.push(strippedRow);
    }
    
    console.log(JSON.stringify(strippedMap));
};

MapBuilder.prototype.loadMap = function(JSONString) {
    var mapFile;
    try {
        mapFile = JSON.parse(JSONString);
        this.map = [];
        var mapFileRow = [],
            mapRow = [];
        for (var y = 0; y < mapFile.length; y ++) {
            mapFileRow = mapFile[y];
            mapRow = [];
            for (var x = 0; x < mapFileRow.length; x++) {
                mapRow.push(new Tile(mapFileRow[x].x, mapFileRow[x].y, mapFile.length, mapFileRow[x].h, mapFileRow[x].type, mapFileRow[x].walkable));
            }
            this.map.push(mapRow);
        }
        $("#numX").val(this.map[0].length);
        $("#numY").val(this.map.length);
        this.Draw();
    } catch (e) {
        
    }
    $("#loadDialog").addClass("hidden");
}

function getWidth() {
    if (self.innerWidth) {
       return self.innerWidth;
    }
    else if (document.documentElement && document.documentElement.clientWidth){
        return document.documentElement.clientWidth;
    }
    else if (document.body) {
        return document.body.clientWidth;
    }
    return 0;
}
function getHeight() {
    if (self.innerHeight) {
       return self.innerHeight;
    }
    else if (document.documentElement && document.documentElement.clientHeight){
        return document.documentElement.clientHeight;
    }
    else if (document.body) {
        return document.body.clientHeight;
    }
    return 0;
}